my_array = [10, 11, 12, 13, 15]
# print(my_array)
# print(my_array)
my_array.append(17)
del my_array[2]


# print(my_array)
# print(my_array[0:3])


class Point:
    x = int
    y = int

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __str__(self):
        return f'(x: {self.x}, y: {self.y}) '


class Polyline:
    points = [Point]

    def __init__(self, points=None):
        if points is None:
            points = []
        self.points = points

    def add(self, x, y):
        self.points.append(Point(x, y))

    def add_point(self, point):
        self.points.append(point)

    def __str__(self):
        point_str = ""
        for point in self.points:
            point_str += point.__str__()
        return point_str


p1 = Point(1, 2)
p2 = Point(2, 3)
p3 = Point(3, 4)
p4 = Point(4, 5)
p5 = Point(5, 6)

poly = Polyline()

poly.add_point(p1)
poly.add_point(p2)
poly.add_point(p3)
poly.add_point(p4)
poly.add_point(p5)
poly.add(8, 9)

print(poly.__str__())
