import tkinter as tk
from operation import Operation

win = tk.Tk()

result = tk.StringVar()
result.set("...")


def addition(a_entry: tk.Entry, b_entry: tk.Entry):
    res = Operation(int(a_entry.get()), int(b_entry.get())).add()
    result.set(res)


def soustraction(a_entry: tk.Entry, b_entry: tk.Entry):
    res = Operation(int(a_entry.get()), int(b_entry.get())).sous()
    result.set(res)


def multiplication(a_entry: tk.Entry, b_entry: tk.Entry):
    res = Operation(int(a_entry.get()), int(b_entry.get())).mul()
    result.set(res)


def division(a_entry: tk.Entry, b_entry: tk.Entry):
    res = Operation(int(a_entry.get()), int(b_entry.get())).div()
    result.set(res)


# Label of A:
a_label = tk.Label(text='A: ')
a_label.pack()

# Entry of A:
a_entry = tk.Entry(win)
a_entry.pack()

# Label of B:
b_label = tk.Label(text='B: ')
b_label.pack()

# Entry of B:
b_entry = tk.Entry(win)
b_entry.pack()

# Addition Button:
btn_add = tk.Button(text='Addition')
btn_add.bind('<Button-1>', lambda event, a=a_entry,
                                  b=b_entry: addition(a, b))
btn_add.pack()

# Soustraction Button:
sous = tk.Button(text='Soustraction')
sous.bind('<Button-1>', lambda event, a=a_entry,
                               b=b_entry: soustraction(a, b))
sous.pack()

# Multiplication Button:
mul = tk.Button(win, text='Multiplication')
mul.bind('<Button-1>', lambda event, a=a_entry,
                               b=b_entry: multiplication(a, b))
mul.pack()

# Division Button:
div = tk.Button(win, text='Division')
div.bind('<Button-1>', lambda event, a=a_entry,
                               b=b_entry: division(a, b))
div.pack()

# Resultat:
resultat_label = tk.Label(win, text='Resultat : ')
resultat_label.pack()

result = tk.StringVar()
result.set("...")
resultat_variable = tk.Label(win, textvariable=result)
resultat_variable.pack()
win.mainloop()
