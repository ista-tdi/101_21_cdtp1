from User import User
from Commande import Commande


# Methode classique dyal Getter/Setter
class Client(User):
    # Daorir mn declaration dyal les attributes 9bal __init__
    # Darori ya imma ta3ti type (str, int, float, Objet, ...) Olla dir initialisation ('', 0, [], (), ....)
    __status: str
    __commande_list: [Commande]

    def __init__(self, id, nom, cin, role, status='NON_ACTIVE'):
        super().__init__(id, nom, cin, role)
        # Hna rani 3ayat 3la setter machi attribut __status ola __command_list
        self.status = status
        self.commande_list = []

    # Getter
    @property
    def status(self):
        return self.__status

    # Setter
    @status.setter
    def status(self, status):
        self.__status = status

        # Getter
    @property
    def commande_list(self):
        return self.__commande_list

    # Setter
    @commande_list.setter
    def commande_list(self, commande_list):
        self.__commande_list = commande_list
