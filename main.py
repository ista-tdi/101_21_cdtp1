# 1 - Class creation
# The class should be uppercase
# class darori tkon maj wa 2illa matlomo gha rasskom
# Ghir l7arf lawal li maj
class Ordinateur:

    # hada syntax dyal "Constructeur" / Bach tsawbo "instances"
    # Bnisba les attribues li ghaykono facultatif ghadi tl9aw 7dahom "="
    # b7al etat valeur par def hya FALSE
    def __init__(self, fabricateur, serie, pouce=15, mType="Post", etat=False):
        # self.fabricateur: fabricateur dyal la classe
        # fabricateur: fabricateur li kayn en parametre
        self.fabricateur = fabricateur
        self.serie = serie
        self.pouce = pouce
        self.mType = mType
        self.etat = etat

    def demarrer(self):
        if not self.etat:
            self.etat = True

    def arreter(self):
        if self.etat:
            self.etat = False

    def afficher_status(self):
        if self.etat:
            print("L'etat de l'ordinateur est Allumé: ")
        else:
            print("L'etat de l'ordinateur est Arreter: ")

# Hna creation des objets

hpProBook = Ordinateur("HP", "Probook360", 13,)
hpElitBook = Ordinateur("HP", "Elitbook360", 17, "Portable", True)
asusRog = Ordinateur("ASUS", "ROG-GL554", 17, "PC", True)
acer = Ordinateur("ACER", "ASPIRE15", 15, "PC")


# Hna fin tan3yat 3la methodes
hpProBook.afficher_status()
hpProBook.demarrer()
hpProBook.afficher_status()
hpProBook.arreter()
hpProBook.afficher_status()
