class Cercle:
    def __init__(self, rayon):
        self.__rayon = rayon

    def surface(self):
        return (self.__rayon ** 2) * 3.14


class Cylindre(Cercle):
    def __init__(self, rayon, hauteur):
        super().__init__(rayon)
        self.__hauteur = hauteur

    def volume(self):
        return super().surface() * self.__hauteur

    def afficher(self):
        print(f'La surface est: {self.surface()} -- Volume est: {self.volume()}')


cylindre1 = Cylindre(4, 10)

suf = cylindre1.surface()
vol = cylindre1.volume()

print(suf)
print(vol)
cylindre1.afficher()
