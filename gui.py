import tkinter as tk


def handle_keypress(event):
    """Print the character associated to the key pressed"""
    print("handle_keypress: ", event)
    if event.keycode == 8:
        delete_value()


def handle_click(event):
    """Print the character associated to the key pressed"""
    print("Button Clicked")
    print(entry.get())


def delete_value():
    """Print the character associated to the key pressed"""
    print("Value Deleted")
    print(entry.delete(0, 3))


# Create Form/Window
win = tk.Tk()

# Add Your Widget
label = tk.Label(text="Label")
# Pack widget/ Active Widget
label.pack()

button = tk.Button(name="t", text="My Button")
button.bind("<Button-1>", handle_click)
# Pack widget/ Active Widget
button.pack()

entry = tk.Entry(text="MyInput")
entry.pack()

entry.bind("<Key>", handle_keypress)
# Stop Window
win.mainloop()
