class InvalidPhoneException(Exception):
    def __init__(self, msg):
        super().__init__("InvalidPhoneException-" + msg)
