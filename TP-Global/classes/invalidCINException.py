class InvalidCINException(Exception):
    def __init__(self, msg):
        super().__init__("InvalidCINException-" + msg)
