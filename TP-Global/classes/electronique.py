from abc import ABC

from .produit import Produit


class Electronique(Produit, ABC):
    __type: str

    def __init__(self, id_prod, nom, prix, type):
        super().__init__(id_prod, nom, prix)
        self.type = type

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, type):
        self.__type = type

    def discount(self):
        super().prix = super().prix * 0.95

    def __str__(self):
        return 'Electronique {\n' + super().__str__() + f'\t Type: {self.type}\n' \
                                                        '\t}'
