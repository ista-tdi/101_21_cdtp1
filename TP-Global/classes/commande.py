from datetime import datetime


class Produit:
    def __str__(self):
        return '<P>'


class Commande:
    __id_c: str
    __produit: Produit
    __prix_total: str
    __date_creation: datetime

    def __init__(self, id_c, produit: Produit):
        self.id_c = id_c
        self.produit = produit
        self.prix_total = 0
        self.date_creation = datetime.now()

    @property
    def id_c(self):
        return self.__id_c

    @id_c.setter
    def id_c(self, id_c):
        self.__id_c = id_c

    @property
    def produit(self):
        return self.__produit

    @produit.setter
    def produit(self, produit):
        self.__produit = produit

    @property
    def prix_total(self):
        return self.__prix_total

    @prix_total.setter
    def prix_total(self, prix_total):
        self.__prix_total = prix_total

    @property
    def date_creation(self):
        return self.__date_creation

    @date_creation.setter
    def date_creation(self, date_creation):
        self.__date_creation = date_creation

    def __str__(self):
        return 'Commande {\n' \
               f'\t ID_C: {self.id_c}\n' \
               f'\t Produit: ' + self.produit.__str__().replace('\t', '\t\t') + '\n' \
               f'\t PrixTotal: {self.prix_total.__str__()}\n' \
               f'\t DateCreation: {self.date_creation.__str__()}\n' \
               '\t}'
