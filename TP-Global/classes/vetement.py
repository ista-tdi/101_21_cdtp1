from .produit import Produit


class Vetement(Produit):
    __type: str
    __couleur: str
    __taille: float

    def __init__(self, id_prod, nom, prix, type, couleur, taille):
        super().__init__(id_prod, nom, prix)
        self.type = type
        self.couleur = couleur
        self.taille = taille

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, type):
        self.__type = type

    @property
    def couleur(self):
        return self.__couleur

    @couleur.setter
    def couleur(self, couleur):
        self.__couleur = couleur

    @property
    def taille(self):
        return self.__taille

    @taille.setter
    def taille(self, taille):
        self.__taille = taille

    def discount(self):
        super().prix = super().prix * 0.8

    def __str__(self):
        return 'Vetement {\n' + super().__str__() + f'\t Type: {self.type}\n' \
                                                    f'\t Couleur: {self.couleur}\n' \
                                                    f'\t Taille: {self.taille}\n' \
                                                    '\t}'
