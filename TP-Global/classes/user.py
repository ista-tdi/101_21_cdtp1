import re
from .invalidCINException import InvalidCINException


class User:
    __id = None
    __nom: str
    __cin: str
    __role: str

    def __init__(self, id, nom, cin, role='c'):
        self.id = id
        self.nom = nom
        self.cin = cin
        self.role = role

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def nom(self):
        return self.__nom

    @nom.setter
    def nom(self, nom):
        self.__nom = nom

    @property
    def cin(self):
        return self.__cin

    @cin.setter
    def cin(self, cin):
        pattern = '^A\d{4}$'
        if re.match(pattern, cin):
            self.__cin = cin
        else:
            raise InvalidCINException("invalid cin")

    @property
    def role(self):
        return self.__role

    @role.setter
    def role(self, role):
        if role == 'f':
            self.__role = 'f'
        else:
            self.__role = 'c'

    def __str__(self):
        return f'\t ID: {self.id}\n' \
               f'\t ID: {self.nom}\n' \
               f'\t ID: {self.cin}\n' \
               f'\t ID: {self.role}\n'
