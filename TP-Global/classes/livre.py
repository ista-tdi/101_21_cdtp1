from .papier import Papier


class Livre(Papier):
    __autheur: float
    __titre: str

    def __init__(self, id_prod, nom, prix, type_qualite, epesseur, autheur):
        super().__init__(id_prod, nom, prix, type_qualite, epesseur)
        self.autheur = autheur
        self.titre = nom

    @property
    def autheur(self):
        return self.__autheur

    @autheur.setter
    def autheur(self, autheur):
        self.__autheur = autheur

    @property
    def titre(self):
        return self.__titre

    @titre.setter
    def titre(self, titre):
        self.__titre = titre

    def discount(self):
        super().prix = super().discount() * 0.8

    def change_qualite(self):
        super().type_qualite += 1

    def __str__(self):
        return 'Livre {\n' + super().__str__() + f'\t Auteur: {self.autheur}\n' \
                                                 f'\t Titre: {self.titre}\n' \
                                                 '\t}'
