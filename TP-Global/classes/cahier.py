from .papier import Papier


class Cahier(Papier):
    __page: str

    def __init__(self, id_prod, nom, prix, type_qualite, epesseur, page):
        super().__init__(id_prod, nom, prix, type_qualite, epesseur)
        self.page = page

    @property
    def page(self):
        return self.__page

    @page.setter
    def page(self, page):
        self.__page = page

    def discount(self):
        super().prix = super().discount() * 0.95

    def change_qualite(self):
        super().type_qualite += 1

    def __str__(self):
        return 'Cahier {\n' + super().__str__() + f'\t Page: {self.page}\n' \
                                                  '\t}'
