from .user import User
import re
from .invalidPhoneException import InvalidPhoneException
from .produit import Produit


class Fourniseur(User):
    __adresse: str
    __service: str
    __fix: str
    __products: [Produit]

    def __init__(self, id, nom, cin, role, adresse, service, fix):
        super().__init__(id, nom, cin, role)
        self.adresse = adresse
        self.service = service
        self.fix = fix
        self.produits = []

    @property
    def adresse(self):
        return self.__adresse

    @adresse.setter
    def adresse(self, adresse):
        self.__adresse = adresse

    @property
    def service(self):
        return self.__service

    @service.setter
    def service(self, service):
        self.__service = service

    @property
    def fix(self):
        return self.__fix

    @fix.setter
    def fix(self, fix):
        pattern = '^05 \d{2} \d{2} \d{2} \d{2}$'
        if re.match(pattern, fix):
            self.__fix = fix
        else:
            raise InvalidPhoneException('invalid fix')

    @property
    def produits(self):
        return self.__produits

    @produits.setter
    def produits(self, produits):
        self.__produits = produits

    def ajouter_produit(self, produit: Produit):
        self.produits.append(produit)

    def __str__(self):
        str = 'Fournissuer {\n' + super().__str__() + f'\t Adresse: {self.adresse}\n' \
                                                      f'\t Service: {self.service}\n' \
                                                      f'\t Fix: {self.fix}\n' \
                                                      f'\t Produits: [\n'
        for prod in self.produits:
            str += f'\t\t ' + prod.__str__().replace('\t', '\t\t\t') + "\n"

        return str + '\t]\n}'
