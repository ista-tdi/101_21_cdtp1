from .user import User
from .commande import Commande


class Client(User):
    __status: str
    __commande_list: [Commande]

    def __init__(self, id, nom, cin, role, status='NON_ACTIVE'):
        super().__init__(id, nom, cin, role)
        self.status = status
        self.commande_list = []

    @property
    def commande_list(self):
        return self.__commande_list

    @commande_list.setter
    def commande_list(self, commande_list):
        self.__commande_list = commande_list

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, status):
        if status == 'ACTIVE' or status == 'MOYEN':
            self.__status = status
        else:
            self.__status = 'NON_ACTIVE'

    def ajoute_commande(self, commande):
        self.__commande_list.append(commande)

    def __str__(self):
        str = 'Client {\n' + super().__str__() + f'\t Status: {self.status}\n\t[\n'

        for command in self.commande_list:
            str += f'\t' + command.__str__().replace('\t', '\t\t') + "\n"

        return str + "\t]\n}"
