from abc import ABC, abstractmethod
from .produit import Produit


class Papier(Produit):
    __type_qualite: str
    __epesseur: float

    def __init__(self, id_prod, nom, prix, type_qualite, epesseur):
        super().__init__(id_prod, nom, prix)
        self.type_qualite = type_qualite
        self.epesseur = epesseur

    @property
    def type_qualite(self):
        return self.__type_qualite

    @type_qualite.setter
    def type_qualite(self, type_qualite):
        self.__type_qualite = type_qualite

    @property
    def epesseur(self):
        return self.__epesseur

    @epesseur.setter
    def epesseur(self, epesseur):
        self.__epesseur = epesseur

    def discount(self):
        super().prix = super().prix * 0.9
        return super().prix

    @abstractmethod
    def change_qualite(self):
        pass

    def __str__(self):
        return super().__str__() + f'\t TypeQualité: {self.type_qualite}\n' \
                                   f'\t TypeQualité: {self.epesseur}\n' \
