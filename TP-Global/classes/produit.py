from abc import ABC, abstractmethod


class Produit(ABC):
    __id_prod: str
    __nom: str
    __prix: float

    def __init__(self, id_prod, nom, prix):
        self.id_prod = id_prod
        self.nom = nom
        self.prix = prix

    @property
    def id_prod(self):
        return self.__id_prod

    @id_prod.setter
    def id_prod(self, id_prod):
        self.__id_prod = id_prod

    @property
    def nom(self):
        return self.__nom

    @nom.setter
    def nom(self, nom):
        self.__nom = nom

    @property
    def prix(self):
        return self.__prix

    @prix.setter
    def prix(self, prix):
        self.__prix = prix

    @abstractmethod
    def discount(self):
        pass

    def __str__(self):
        return f'\t ID_PRD: {self.id_prod}\n' \
               f'\t ID_PRD: {self.nom}\n' \
               f'\t PRIX: {self.prix}\n'
