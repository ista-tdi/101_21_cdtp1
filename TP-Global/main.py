from classes.fournisseur import Fourniseur
from classes.produit import *
from classes.client import *
from classes.user import *
from classes.papier import *
from classes.commande import *
from classes.livre import *
from classes.vetement import *
from classes.cahier import *
from classes.electronique import *
from classes.invalidCINException import *
from classes.invalidPhoneException import *



# --------------------------------------
# Creation D'un Fournisseur
# --------------------------------------
f1 = Fourniseur(1, "F-Mohamed", "A1234", "f", "Addr", "Info", "05 00 00 00 00")

# Daba fl-assl khassni ndir gestion des exceptiosn machi n7at hadchi haka nichan
# Khasni nkon dayr chi7aja b7al haka

print("*********************************")
print("******* FOURNISSEUR 2 ***********")
try:
    f2 = Fourniseur(1, "F-Mohamed", "A1234", "f", "Addr", "Info", "06 00 00 00 00")

    sandala = Vetement(id_prod=4, nom='SANDALA',
                       prix=100, type="SANDAL", couleur="Bleue", taille="M")

    # ****** 10dhs 3awtani ******
    livre_3ab9ari = Livre(id_prod=1, nom='Kayfa Takon 3ab9ari fi 3 ayyam bidon Ostad SALHI',
                                 prix=10, type_qualite=1, epesseur=4, autheur="SALHI")

    f2.ajouter_produit(sandala)
    f2.ajouter_produit(livre_3ab9ari)

    print(f2.__str__())

except InvalidPhoneException:
    print("Invalid Phone")
except InvalidCINException:
    print("Invalid CIN")
finally:
    print("*********************************\n\n")

# --------------------------------------
# Creation des produits
# --------------------------------------
'''
    Man9darch n instancier (creation) dyal chi objet mn chi class Abstrait
    Ya3ni bach ndir chi wa7da mn hado raha impossible
      prod = Produit(......)
      pap = Papier(....)
    7it Produit o Papier rahom des classes abstraites
    Bach ndir instance mn had les objets khassni ndir
      instance mn les fils li homa classe dyalhom wmaykonoch abstraite
      TFAHMNA ???????????????? Bach nktb hadchi b lfrancer ghangol:
          'On ne peux pas créer un instance à partir d'une classe abstrait'
      Daba wad7a had lfrancer yak ????????????????????????????
'''

# Mnin tayktro 3likom les attributs diro smia o7to valeur 7daha bach matlfoch
# (************ prix = 10dhs --- hadik li kant tantsal likom lbar7 ************)
livre_salhi_sciences = Livre(id_prod=1, nom='Les sciences de SALHI',
               prix=10, type_qualite=2, epesseur=4, autheur="SALHI")

livre_hasni_exeperiences = Livre(id_prod=2, nom='Les experiences de HASNI',
               prix=100, type_qualite=3, epesseur=2, autheur="HASNI")

cahier_1 = Cahier(id_prod=3, nom='Cahier 50 pages',
               prix=2.5, type_qualite=1, epesseur=1, page=50)

chemise = Vetement(id_prod=4, nom='Chemise Classe',
               prix=40, type="Chemise", couleur="Bleue", taille="M")

t_shirt = Vetement(id_prod=5, nom='T-Shirt Homme',
               prix=30, type="T-Shirt", couleur="Blanc", taille="S")

smart_phone = Electronique(id_prod=6, nom='101 SAMRT',
               prix=6000, type="SMARTPHONE")


# --------------------------------------
# Affecter les produits à un Fournisseur
# --------------------------------------
f1.ajouter_produit(livre_salhi_sciences)
f1.ajouter_produit(livre_hasni_exeperiences)
f1.ajouter_produit(cahier_1)
f1.ajouter_produit(t_shirt)
f1.ajouter_produit(smart_phone)

print("*********************************")
print("******* FOURNISSEUR 1 ***********")
print(f1.__str__())
print("*********************************\n")

try:
    client = Client(1, "Mohamed", "A1234", "C")
    cmd1 = Commande(1, None)
    cmd2 = Commande(2, None)
    cmd3 = Commande(1, None)
    cmd4 = Commande(2, None)

    client.ajoute_commande(cmd1)
    client.ajoute_commande(cmd2)
    client.ajoute_commande(cmd3)
    client.ajoute_commande(cmd4)

    print("*********************************")
    print("************ CLIENT 1 ***********")
    print(client.__str__())
    print("*********************************\n\n")

except InvalidCINException:
    print("Hadchi mam9adch 3ndk")