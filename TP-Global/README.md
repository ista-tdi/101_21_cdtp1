# IMPORTANT
## 1. Afficher
Kola class darori tsawbo liha tari9a bach ghada t afficha
f exam rah ghadi ngolik sawb lia methode ```__str__```

Had méthode rah tatkon 3la had chakila dima
```python
def __str__(self):
    return f'Kifach bghito dakchi dyalkom iban'
```

_EXEMPLE:_
3andi Class 
```python
class Person:
    nom : str
    age: int

    def __str__:
        return f'Person> Nom: {self.nom} - Age: {self.age}'
```
Hada ghir mital osafi dyal exemple kifach nsawb ```__str__```

Oraddo bal rani dart ```return``` maaaaaaachi ```print```

daba ila bghit n3ayat 3la hadchi andir haka par exemple:
```python
p = Person()

# Haaaaaaaaa kifach tansta3malha ila tlabt manak
# t affichier f console
print(p.__str())
```

**Chofo les ```__str__`` li sawbt** 

**Ochofo fichier** ```result.txt```ila ma9ditoch t executiw program 
Rani dart fih resultat dyal chno ghadi iban

## 2. Encapsulation
Encapsultation hya annaka traja3 kolchi privé ( ```__monAttribut__```) 
Omanba3d dir les ```Getters```/```Setters```
### 2.1 ```Getters```/```Setters``` Méthode classique
**Tatsawb les attributes dyalk 3aaaaaaaadi kolhom privé** 
Okola wa7d tadir lih méthode tatbda b 
- ```get_``` bnisba l ```Getters``` 
- ```set_``` bnisba l ```Setters```
- 
**Exemple:**
```python
class Person:
    __nom : str
    __age: int

    def get_nom(self):
        return self.__nom
    
    def set_nom(self, nom):
        self.__nom = nom
```

Wila bghiti tsawb ```__init__``` adir lih b7al haka

```python
    def __init(self, nom, age):
        self.set_nom(nom)
        self.set_age(age)
```

### 2.2 ```Getters```/```Setters``` Méthode avec ```Property```
**Tatsawb les attributes dyalk 3aaaaaaaadi kolhom privé** 
Okola wa7d tadir lih méthode bnafs smiya dyal attribut bla dok ```__``` 

**Exemple:**
```python
class Person:
    __nom : str
    __age: int

    # GETTER
    @property
    def nom(self):
        return self.__nom
    
    # Setter
    @nom.setter
    def nom(self, nom):
        self.__nom = nom
```

Wila bghiti tsawb ```__init__``` adir lih b7al haka

```python
    def __init(self, nom, age):
        self.nom = nom
        self.age = age
```


## 3. Les classe li darna
Bnisba les classe b7al ```Fournisseur```, ```Client```, ...
Khass tzid wa7d methode smitha ```ajouter_chi7aha``` 

Had ```ajouter_``` hya li tat3mr lik list dyalk li wast manhom


