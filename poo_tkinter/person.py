class Person:
    def __init__(self, nom):
        self.nom = nom
        self.km = 0

    def marcher(self, hours):
        self.km += hours * 3
        return self.km

    def get_km(self):
        return self.km

    def set_km(self, km):
        self.km = km

    def reset(self):
        self.km = 0

    def __str__(self):
        return f'Nom: {self.nom} ' \
               f'-- KM: {self.km}'
