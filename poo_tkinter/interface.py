import tkinter as tk
from poo_tkinter.person import Person

person_list = []


def list_to_str():
    s = ""
    for p in person_list:
        s += p.__str__() + "\n"
    return s


def ajouter_person_click(entry_name: tk.Entry):
    name = entry_name.get()
    p = Person(name)
    print(f"Ajouter Person {p.nom}")
    person_list.append(p)
    entry_name.delete(0, len(entry_name.get()))


def marcher_click(entry_name: tk.Entry, entry_hours: tk.Entry, km_text):
    h = entry_hours.get()
    name = entry_name.get()

    for index, p in enumerate(person_list):
        if p.nom == name:
            if h is not None or h != '':
                km = p.marcher(int(h))
                km_text.set(km)
                person_list[index] = p
                list_text.set(list_to_str())

win = tk.Tk()
km_text = tk.StringVar()
km_text.set("--")

list_text = tk.StringVar()
list_text.set(list_to_str())


label_nom = tk.Label(text="NOM: ")
label_nom.pack()
entry_nom = tk.Entry()
entry_nom.pack()

btn_ajouter = tk.Button(text="Ajouter")
btn_ajouter.bind("<Button-1>", lambda event, entry_name=entry_nom: ajouter_person_click(entry_name))
btn_ajouter.pack()

label_hours = tk.Label(text="Hours: ")
label_hours.pack()

entry_hours = tk.Entry()
entry_hours.pack()

val_km = tk.Label(textvariable=km_text)

btn_marcher = tk.Button(text="Marcher")
btn_marcher.bind("<Button-1>",
                 lambda event, entry_nom=entry_nom, entry_hours=entry_hours, km_text=km_text: marcher_click(entry_nom,
                                                                                                            entry_hours,
                                                                                                            km_text))
btn_marcher.pack()

label_km = tk.Label(text="KM: ")
label_km.pack()
val_km.pack()

label_person_list = tk.Label(textvariable=list_text)
label_person_list.pack()

win.mainloop()
